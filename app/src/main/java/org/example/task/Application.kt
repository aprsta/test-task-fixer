package org.example.task

import org.example.task.common.di.Component
import org.example.task.common.di.ContextModule
import org.example.task.common.di.DaggerComponent

class Application : android.app.Application() {
    companion object {
        lateinit var component: Component
            private set
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerComponent.builder()
                .contextModule(ContextModule(this))
                .build()
    }
}