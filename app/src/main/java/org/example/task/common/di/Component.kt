package org.example.task.common.di

import dagger.Component
import org.example.task.mvvm.observable.viewmodel.ConverterViewModel
import javax.inject.Singleton

@Component(modules = [NetworkModule::class, ContextModule::class])
@Singleton
interface Component {
    fun inject(viewModel: ConverterViewModel)
}