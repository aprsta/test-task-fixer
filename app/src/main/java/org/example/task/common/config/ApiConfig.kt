package org.example.task.common.config

object ApiConfig {
    const val FIXER_API_PROTOCOL = "http"
    const val FIXER_API_HOST = "data.fixer.io"
    const val FIXER_API_PORT = 80
    const val FIXER_URL = "$FIXER_API_PROTOCOL://$FIXER_API_HOST:$FIXER_API_PORT"

    // FIXME: Get the real API key and move it to the xml file.
    const val FIXER_API_KEY = "cb7a4bee6c5d8a26a20032f40d9c5d73"

    const val HEADER_ACCEPT = "Accept: application/json"
    const val HEADER_CONTENT = "Content-Type: application/json"

    const val TIMEOUT_MILLISEC: Long = 5000
    const val CACHE_SIZE: Long = 2 * 1024 * 1024
}