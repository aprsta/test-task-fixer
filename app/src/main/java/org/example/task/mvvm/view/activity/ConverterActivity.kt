package org.example.task.mvvm.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Spinner
import org.example.task.R
import org.example.task.databinding.ActivityConverterBinding
import org.example.task.mvvm.model.data.Currency
import org.example.task.mvvm.observable.viewmodel.ConverterViewModel


class ConverterActivity : AppCompatActivity() {
    private lateinit var viewModel: ConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityConverterBinding>(this, R.layout.activity_converter)
        viewModel = ViewModelProviders.of(this).get(ConverterViewModel::class.java)
        binding.viewModel = viewModel

        val adapter = ArrayAdapter.createFromResource(this, R.array.converter_activity_spinner_items, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        findViewById<Spinner>(R.id.spinnerCurrencyFrom).apply {
            this.adapter = adapter
            this.setSelection(if (viewModel.converterCurrencyFrom == Currency.USD) 0 else 1)
        }

        findViewById<Spinner>(R.id.spinnerCurrencyTo).apply {
            this.adapter = adapter
            this.setSelection(if (viewModel.converterCurrencyTo == Currency.USD) 0 else 1)
        }

        viewModel.observeAll(this)

        if (savedInstanceState == null || viewModel.converterRateUsd == null || viewModel.converterRateSek == null) {
            viewModel.update()
        }
    }
}
