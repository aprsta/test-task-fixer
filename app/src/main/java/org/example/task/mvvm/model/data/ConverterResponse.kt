package org.example.task.mvvm.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ConverterResponse(
        @SerializedName("success")
        @Expose
        val success: Boolean? = null,

        @SerializedName("rates")
        @Expose
        val rates: ConverterRates? = null
)