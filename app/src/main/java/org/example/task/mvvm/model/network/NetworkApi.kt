package org.example.task.mvvm.model.network

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import org.example.task.common.config.ApiConfig
import org.example.task.mvvm.model.data.ConverterResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetworkApi(private val fixerApi: FixerApi) {
    companion object {
        private val TAG: String = NetworkApi::class.java.simpleName
    }

    fun asyncGetRates(liveData: MutableLiveData<ConverterResponse>) {
        fixerApi.getRates(ApiConfig.FIXER_API_KEY).enqueue(object : Callback<ConverterResponse> {
            override fun onResponse(call: Call<ConverterResponse>?, response: Response<ConverterResponse>?) {
                if (response == null || !response.isSuccessful || response.body() == null || response.body().success != true) {
                    Log.w(TAG, "[asyncGetRates] Error while getting rates. Request: '" + call?.request().toString() + "'. Code: '" + response?.code() + "'")

                    // TODO: Return error code in liveData to detail the error message.
                    liveData.postValue(null)
                    return
                }

                liveData.postValue(response.body())
            }

            override fun onFailure(call: Call<ConverterResponse>?, throwable: Throwable?) {
                Log.w(TAG, "[asyncGetRates] Failure while getting rates. Request: '" + call?.request().toString() + "'. Throws: '" + throwable?.message + "'")

                // TODO: Separate a NoConnection exception from others and return it in the liveData.
                liveData.postValue(null)
            }
        })
    }
}