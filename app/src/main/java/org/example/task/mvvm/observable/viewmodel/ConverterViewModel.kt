package org.example.task.mvvm.observable.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.Bindable
import android.support.v4.app.SupportActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import org.example.task.Application
import org.example.task.BR
import org.example.task.common.util.ObservableViewModel
import org.example.task.mvvm.model.data.ConverterResponse
import org.example.task.mvvm.model.data.Currency
import org.example.task.mvvm.model.network.NetworkApi
import javax.inject.Inject

class ConverterViewModel : ObservableViewModel() {
    /** Section: Injects. */

    @Inject
    lateinit var networkApi: NetworkApi

    /** Section: Bindable properties. */

    var converterRateUsd: Double? = null
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterRateUsd)
        }

    var converterRateSek: Double? = null
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterRateSek)
        }

    var converterAmountFrom: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterAmountFrom)
        }

    var converterAmountFromDouble: Double? = null
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterAmountFromDouble)
        }

    var converterCurrencyFrom: Currency = Currency.USD
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterCurrencyFrom)
        }

    var converterAmountTo: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterAmountTo)
        }

    var converterAmountToDouble: Double? = null
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterAmountToDouble)
        }

    var converterCurrencyTo: Currency = Currency.SEK
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterCurrencyTo)
        }

    var converterLoading: Boolean = false
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.converterLoading)
        }

    /** Section: Live data. */

    private val converterResponseLiveData = MutableLiveData<ConverterResponse>()

    /** Section: Constructor. */

    init {
        Application.component.inject(this)
    }

    /** Section: Utility methods. */

    fun observeAll(activity: SupportActivity) {
        converterResponseLiveData.observe(activity, Observer {
            converterLoading = false

            if (it?.rates == null) {
                return@Observer
            }

            converterRateUsd = it.rates.usd
            converterRateSek = it.rates.sek

            calculateIfPossible()
        })
    }

    /** Section: Public methods. */

    fun update() {
        converterLoading = true

        networkApi.asyncGetRates(converterResponseLiveData)
    }

    /** Section: Private methods. */

    private fun calculateIfPossible() {
        if (converterRateUsd == null || converterRateSek == null || converterAmountFromDouble == null) {
            return
        }

        if (converterCurrencyFrom == converterCurrencyTo) {
            converterAmountToDouble = converterAmountFromDouble
            converterAmountTo = converterAmountToDouble.toString()
            return
        }

        if (converterCurrencyFrom == Currency.USD) {
            converterAmountToDouble = converterAmountFromDouble!! / converterRateUsd!! * converterRateSek!!
            converterAmountTo = String.format("%.2f", converterAmountToDouble)
            return
        }

        if (converterCurrencyFrom == Currency.SEK) {
            converterAmountToDouble = converterAmountFromDouble!! / converterRateSek!! * converterRateUsd!!
            converterAmountTo = String.format("%.2f", converterAmountToDouble)
            return
        }
    }

    /** Section: Watchers and Listeners. */

    val converterSpinnerFromItemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            converterCurrencyFrom = if (position == 0) Currency.USD else Currency.SEK
            calculateIfPossible()
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    val converterSpinnerToItemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            converterCurrencyTo = if (position == 0) Currency.USD else Currency.SEK
            calculateIfPossible()
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    val converterEditTextFromWatcher: TextWatcher = object : TextWatcher {
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(value: Editable) {
            converterAmountFrom = value.toString()
            converterAmountFromDouble = converterAmountFrom.toDoubleOrNull()

            converterAmountTo = ""
            converterAmountToDouble = null

            calculateIfPossible()
        }
    }
}