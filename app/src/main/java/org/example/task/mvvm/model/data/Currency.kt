package org.example.task.mvvm.model.data

enum class Currency {
    USD, SEK
}