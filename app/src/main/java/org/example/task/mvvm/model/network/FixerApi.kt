package org.example.task.mvvm.model.network

import org.example.task.common.config.ApiConfig
import org.example.task.mvvm.model.data.ConverterResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface FixerApi {
    @Headers(ApiConfig.HEADER_ACCEPT, ApiConfig.HEADER_CONTENT)
    @GET("/api/latest")
    fun getRates(@Query("access_key") apiKey: String): Call<ConverterResponse>
}