package org.example.task.mvvm.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ConverterRates(
        @SerializedName("EUR")
        @Expose
        val eur: Double? = null,

        @SerializedName("SEK")
        @Expose
        val sek: Double? = null,

        @SerializedName("USD")
        @Expose
        val usd: Double? = null
)