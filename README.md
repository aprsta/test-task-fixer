Simple currency converter with the Fixer API usage.

 - Pattern: MVVM
   
 - Libraries: Retrofit, Android Data Bindings, Dagger2
